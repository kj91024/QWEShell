# QWEShell Español
## About
Es un "sandbox" para tu shell en donde tienes control del ssh para servidores web, donde desarrollamos un ambiente donde pueda mantenerse todo un servidor con los mejores softwares que encontramos hoy por hoy en el internet donde la mayoria son de código libre.

Este proyecto es generado por la necesidad de automatizar tareas repetitivas que comunmente hacen los administradores de sistemas conocidos coloquialmente atravez del internet como "sysadmin", esta necesidad de automatizar hace que las acciones de colocar una web, migrar un dominio entero, guardar en backup o incluso poner certificados sean mas sencillos de nunca antes visto. Y todo esto mediante una GUI o panel de administración solo para manejar todo estos softwares.

Cabe recalcar que este proyecto solo USA sofware de terceros y solo es un CONTROLADOR de estos sofware más no un servidor web, un servidor proxy, ftp, etc.

Este codigo ya es grande dentro de mis repositorios, solo que ahora lo estoy haciendo público para que el desarrollo sea mucho más rápida a travez de toda nuestra comunidad. Ademas de adoptar mejores prácticas y limpiar y mejorar todo el código que tengo en el otro repositorio privado.

Este proyecto es modular por lo que cada software que instalamos puede ser facilmente reemplazable o eliminable, por lo que no tengas miedo en tunear como tu quieras tu servidor y hacerlo más como tu necesitas. Solo imagina a este proyecto como un gran adaptador de diversas funcionalidades de que puedes modificar a tu gusto.

## Instalación
git clone https://github.com/kj91024/qweshell.git
git branch release-1.0.0
cd qweshell
chmod +x install.sh
./install.sh

## Notas
- Solo esta disponible para Ubuntu y Centos 7, estamos pensando en Centos 6 pero aún esta en ideas.
- Tu puedes escoger que sofware quieres poner dentro de tu servidor, ya que este sofware esta muy modularizado asi que puedes quitar(ojo que es bajo tu responsabilidad ya que se pueden eliminar los datos que configuraste para tu módulo en particular) y agregar el software que requieras en el momento.
- Puedes actualizar y eliminar el software de una forma muy rápida, sin necesidad de hacer pasos extra.
- Ademas que los modulos que se usa de php, fastphp y lsphp son modificables rápidamente.

## About Sofware que usamos
- Todas las configuraciones de su servidor se encuentra en el puerto *8088*.
- Usamos como servidores web OpenLiteSpeed y NGINX, ya que son los mejores dentro del mundo de "web services", con muchas mejoras sobre el rendimiento y manejo de uso de memoria.
- Usamos PureFTP para el protocolo FTP desde el lado del servidor y como apartado web usamos MonsterFTP. 
- Para el manejo de base de datos PostgreSQL y MariaDB ya que son los más sonados dentro del mundo de base de datos y mira ambos son de open source.
- Para el manejo de Log usamos GoAccess como apartado web y desde consola.
- Para el manejo completo de estadisticas de todo tu servidor usamos NetData.
- Para el manejo de Socket usamos GWSocket.
- Para programar desde tu propio servidor mediante un apartado web usamos Visual Studio Code, si dentro de tu browser.
- Para proteccion de tu servidor FailBan y Latch.
- Como manejador de contraseña usamos la API de LastPass ya que es multi plataforma y es mucho mas conveniente para nosotros que estamos controlando un servidor.
- Para el acceso de SSH a travez de un apartado web usamos *Shell In A Box*.

## Complicaciones
- Dado a la naturaleza de OpenLiteSpeed solo se admite un dominio por puerto es por eso que producira errores dentro de su configuración si apuntas un domino a la misma IP que has dado a otro dominio, es por ello que este incidente lo hemos solucionado añadiendo nginx a la formula para que puedas tener todos tus dominos. para saber en que servicio web esta corriendo tu aplicacion puedes ver el archivo *vhost*
- Este Software solo trata de minimizar su impacto dentro del uso de la memoria, por otro lado es necesario que si sabes o vez que algún módulo usa mucha memoria entonces lo desactives (ya que cada modulo son simplemente un sofware de tercero), desactivas simplemente lo apaga no lo desinstala.
- Durante el desarrollo de sus aplicaciones es conveniente que cada modificación que quiera realizar a su servidor con respecto a los módulos lo haga directamente con el comando de este software *qwe* o mediante su interfaz gráfica, para que no tenga problemas con: .htaccess, versiones de php, certificados ssl, etc.
- Te recomendamos que cambies la contraseña de tu ssh si crees que es muy débil, recuerda que no importa que tan grande es ya que vamos a guardarlo dentro de un administrador de contraseñas.

## Donación
Si deseas donarme algo por este trabajo que he realizado te dejo mi cuenta de paypal kj91024@gmail.com, yo solo soy un programador fullstack que trabaja por el software libre, que no me gusta estar repitiendo las mismas tareas que realizo cuando al momento de instalar y desplegar todo un servicio web. Recuerda que cada centimo me ayuda.

# Si vez que hay algún software que serviría mucho dentro de nuestro "sandbox" para añadir mas caracteristicas a este proyecto no dejes de avisarnos estamos, reseptivos a nuevas ideas y nuevos sofware que sale cada dia, para que nos haga la vida mucho mas fácil.